﻿using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        static async Task<int> Main()
        {
            return await ConsoleUI.MakeMenu();
        }
    }
}