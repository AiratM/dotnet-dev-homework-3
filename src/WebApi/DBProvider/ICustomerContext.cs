using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.DBProvider
{
    public interface ICustomerContext 
    {
        DbSet<Customer> Customers { get; set; }

        int SaveChanges();
    }
}