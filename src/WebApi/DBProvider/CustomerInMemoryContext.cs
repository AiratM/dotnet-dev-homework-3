using Microsoft.EntityFrameworkCore;
using WebApi.Models;
namespace WebApi.DBProvider
{
    public class CustomerInMemoryContext : DbContext, ICustomerContext
    { 
        public DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("CustomersInMemoryDB");
        }

        
    }

}