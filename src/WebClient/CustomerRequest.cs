using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Json;
using System;
using System.Linq;

namespace WebClient
{
    public class CustomerRequest
    {
        private const string url = "https://localhost:5001/";
        public CustomerRequest()
        {
        }

        public static async Task<Customer> GetCustomerAsync(long id)
        {
            var response = await new HttpClient().GetAsync($"{url}customers/{id}");
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return await response.Content.ReadFromJsonAsync<Customer>();
            return null;
        }

        public static async Task<string> PostCustomerAsync()
        {
            var response = await new HttpClient().PostAsJsonAsync($"{url}customers", GenerateRandomCustomer());
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                return await response.Content.ReadAsStringAsync();
            else return "Post error";
        }

        private static Customer GenerateRandomCustomer()
        {
            return new Customer()
            {
                Id = new Random().Next(1, 20),
                Firstname = GenerateRandomString(new Random().Next(3, 8)),
                Lastname = GenerateRandomString(new Random().Next(4, 9))
            };
        }

        private static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[new Random().Next(s.Length)]).ToArray());
        }
    }
}