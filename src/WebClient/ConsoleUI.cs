﻿using System;
using System.Threading.Tasks;

namespace WebClient
{
    public class ConsoleUI
    {
        public static async Task<int> MakeMenu()
        {
            bool flag = true;
            Console.Clear();
            DrawMainMenu();
            while (flag)
            {
                Console.WriteLine("Введите номер команды: ");
                var command = Console.ReadKey();
                switch (command.KeyChar)
                {
                    case '1':
                        {
                            Console.WriteLine("\r\n Введите ID: ");
                            string idString = Console.ReadLine();
                            if (long.TryParse(idString, out long id))
                            {
                                var res = await GetCustomerById(id);
                                Console.WriteLine(res?.ToString() ?? "not found!");
                            }
                            else
                                Console.WriteLine("Введен некорректный ID");
                            break;
                        }
                    case '2':
                        {
                            var res = await CustomerRequest.PostCustomerAsync();
                            Console.WriteLine($"Customer saved with id: {res?.ToString()}" ?? "Error!");
                            break;
                        }
                    case '3':
                        {
                            return 0;
                        }
                    default:
                        break;
                }

            }
            return 0;
        }

        private static void DrawMainMenu()
        {
            Console.WriteLine(@"Доступные команды:
                                1. Запросить данные по ID
                                2. Сгенерировать и направить данные на сохранение.
                                3. Завершение работы программы");
        }

        private static async Task<Customer> GetCustomerById(long id)
        {
            return await CustomerRequest.GetCustomerAsync(id);
        }
    }
}
