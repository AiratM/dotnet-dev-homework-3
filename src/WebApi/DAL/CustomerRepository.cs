using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.DBProvider;
using Microsoft.EntityFrameworkCore;

namespace WebApi.DAL
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly ICustomerContext customersContext;

        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            return await customersContext.Customers.ToListAsync();
        }

        public async Task<Customer> GetCustomerByID(long customerId)
        {
            return await customersContext.Customers.FirstOrDefaultAsync(customer => customer.Id == customerId); 
        }

        
        public async Task<bool> InsertCustomer(Customer customer)
        {
            if (GetCustomerByID(customer.Id).Result is null)
            {
                await customersContext.Customers.AddAsync(customer);
                customersContext.SaveChanges();
                return true;
            }
            return false;
        }

        public CustomerRepository(ICustomerContext _customerContext)
        {
            customersContext = _customerContext;
            InitialData();   
        }

        private void InitialData()
        {
            customersContext.Customers.Add(new Customer
            {
                Id = 1,
                Firstname = "User",
                Lastname = "Useroff"
            });

            customersContext.Customers.Add(new Customer
            {
                Id = 2,
                Firstname = "Test",
                Lastname = "Testeroff"
            });

            customersContext.Customers.Add(new Customer
            {
                Id = 3,
                Firstname = "Admin",
                Lastname = "Adminoff"
            });

            customersContext.Customers.Add(new Customer
            {
                Id = 4,
                Firstname = "Someone",
                Lastname = "Else"
            });

            customersContext.SaveChanges();
        }


    }
}