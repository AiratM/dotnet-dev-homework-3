using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.DAL;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly ICustomerRepository customerRepository;

        [HttpGet("{id:long}")]   
        public async Task<ActionResult> GetCustomerAsync([FromRoute] long id)
        {
            await Task.Delay(new Random().Next(1000,2000));
            var res = await customerRepository.GetCustomerByID(id);
            if (res is null)
                return NotFound();
            return new ObjectResult(res);
        }

        [HttpPost("")]   
        public async Task<ActionResult> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (!await customerRepository.InsertCustomer(customer))
                return Conflict();
            return Ok(customer.Id);
        }

        public CustomerController(ICustomerRepository _customerRepository)
        {
            customerRepository = _customerRepository;
        }
    }
}