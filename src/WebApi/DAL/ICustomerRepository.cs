using System;
using System.Linq;
using System.Collections.Generic;
using WebApi.Models;
using System.Threading.Tasks;


namespace WebApi.DAL
{
    public interface ICustomerRepository
    {
        Task<IEnumerable<Customer>> GetCustomers();

        Task<Customer> GetCustomerByID(long customerId);

        Task<bool> InsertCustomer(Customer customer);
    }
}